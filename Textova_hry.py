import sys
import time
def start():
    print("Je sobotní večer a ty, student vysoké školy se chystáš jít do kina s holkou, která se ti už nějakou dobu líbí, ale ještě nemáš její telefonní číslo.\nChceš na ni zapůsobit a telefonní číslo získat, aby jsi ji mohl dál kontaktovat. Právě stojíš před svojí skříní. Co si oblečeš?\n")
    print("a = Šedé značkové tepláky a černou mikinu.")
    print("b = Rifle a kostkovanou košili")
    print("c = Tmavé kalhoty a značkové triko")
    volba = input()
    if volba == "a":
        teplaky()
    elif volba == "b" or "c":
        rifle()
    else:
        print("Měl jsi vybrat a, b nebo c. Volím za tebe b")
        rifle()
def teplaky():
    print("To snad nemyslíš vážně. Tohle nebude dobrý první dojem.\nDívka tuto skutečnost však přehlíží a ptá se: Nó, tak co jsi vybral?")
    print("a = Krvavý horor")
    print("b = Romantická komedie")
    volba = input()
    if volba == "a":
        horor_teplaky()
    elif volba == "b":
        romantika_teplaky()
    else:
        print("Měl jsi vybrat a nebo b. Volím za tebe b")
        romantika_teplaky()
def horor_teplaky():
    print("Tak to se ti povedlo... Nejen, že jsi se oblékl jako buran, ale ještě jsi tu dívku vystrašil k smrti.\nNezažila horší rande. Ta už se na tebe ve škole ani nepodívá. Konec hry.")
def romantika_teplaky():
    print("Dívce se výběr filmu líbí, zaplatíš za ni lístek?")
    print("a = Ano")
    print("b = Ne")
    volba = input()
    if volba == "a":
        ano_teplaky()
    elif volba == "b":
        ne_teplaky()
    else:
        print("Měl jsi vybrat ano nebo ne. Volím za tebe ano")
        ano_teplaky()
def ano_teplaky():
    print("Vyvíjí se to docela dobře. Dívka si film velice užila a chce doprovodit domů.\nChytneš ji za ruku?")
    print("a = Chytnu")
    print("b = Nechytnu")
    volba = input()
    if volba == "a":
        chytnu_teplaky()
    elif volba == "b":
        nechytnu_teplaky()
    else:
        print("Měl jsi vybrat ano nebo ne. Volím za tebe a")
        chytnu_teplaky()
def ne_teplaky():
    print("Oblečeš se jako buran a ještě neplatíš?\nHa! S tebou holka skončila. Konec hry")
def chytnu_teplaky():
    print("Tvůj styl oblékání ji sice neuchvátil, ale zbytek večera si velice užila.\nProto ti dává své číslo, aby ses jí zítra ozval a ukázal ji, že máš v šatníku i jiné kousky.\nTakže v podstatě výhra, máš co jsi chtěl.")
def nechytnu_teplaky():
    print("Ale né. Začínalo to vypadat tak dobře a ty jsi to celé pokazil.\nDívka jen mává a odchází domů. Samozřejmě číslo ti nesdělila. Konec hry")

def rifle():
    print("Dnes ti to vážně sluší.\nDívka se již při prvním pohledu na tebe usmála a zeptala se: Tak co, na jaký film jsi mě vytáhl? :) ")
    print("a = Krvavý horor")
    print("b = Romantická komedie")
    volba = input()
    if volba == "a":
        horor_rifle()
    elif volba == "b":
        romantika_rifle()
    else:
        print("Měl jsi vybrat a nebo b. Volím za tebe b")
        romantika_rifle()
def romantika_rifle():
    print("Super! Dívka se už nemůže dočkat až se k tobě buded tulit u filmu.\nZaplatíš za ni lístek?")
    print("a = Ano")
    print("b = Ne")
    volba = input()
    if volba == "a":
        ano_rifle()
    elif volba == "b":
        ne_rifle()
    else:
        print("Měl jsi vybrat ano nebo ne. Volím za tebe ano")
        ano_rifle()
def ano_rifle():
    print("Jsi opravdový gentleman. Dívka zatím honotí rande 10/10 a prosí tě o doprovod domů.\nChytneš ji u toho za ruku?")
    print("a = Chytnu")
    print("b = Nechytnu")
    volba = input()
    if volba == "a":
        chytnu_rifle()
    elif volba == "b":
        nechytnu_rifle()
    else:
        print("Měl jsi vybrat ano nebo ne. Volím za tebe a")
        chytnu_rifle()
def ne_rifle():
    print("Tohle dívku trochu překvapilo, ale doufá, že sis zapoměl vzít více hotovosti.\nPo pěkném filmu chce doprovodit domů. Chytneš ji za ruku?")
    print("a = Chytnu")
    print("b = Nechytnu")
    volba = input()
    if volba == "a":
        chytnu_rifle2()
    elif volba == "b":
        nechytnu_rifle2()
    else:
        print("Měl jsi vybrat ano nebo ne. Volím za tebe a")
        chytnu_rifle2()
def chytnu_rifle():
    print("Dívka je z tebe úplně unešená a zve tě k sobě domů na skleničku.\nZbytek nechám na tvé představivosti. Gratuluji, vítězíš!")
def nechytnu_rifle():
    print("Dívce to bylo trochu líto, ale na druhou stranu se jí stydlíni docela líbí, takže ti dává své telefonní číslo.\nTakže v podstatě výhra, máš co jsi chtěl.")
def chytnu_rifle2():
    print("Až na to faux pas u pokladny se večer vydařil. Dívka ti dává své telefonní číslo a odchází.\nTakže v podstatě výhra, máš co jsi chtěl.")
def nechytnu_rifle2():
    print("Nezaplatíš za ni kino a ani se nesnažíš ji chytnout za ruku.\nOčividně nemáš zájem a kvůli tomu ani číslo. Konec hry.")
def horor_rifle():
    print("Dívka sice horory moc nemusí, ale říká si, že to snad překousne. Zaplatíš za ni lístek?")
    print("a = Ano")
    print("b = Ne")
    volba = input()
    if volba == "a":
        ano_rifle1()
    elif volba == "b":
        ne_rifle1()
    else:
        print("Měl jsi vybrat ano nebo ne. Volím za tebe ano")
        ano_rifle1()
def ano_rifle1():
    print("Dívka ocenila, že jsi za ní zaplatil a i když se jí film moc nelíbil, chtěla by doprovodit domů.\nChytneš ji po cestě za ruku?")
    print("a = Chytnu")
    print("b = Nechytnu")
    volba = input()
    if volba == "a":
        chytnu_rifle1()
    elif volba == "b":
        nechytnu_rifle1()
    else:
        print("Měl jsi vybrat ano nebo ne. Volím za tebe a")
        chytnu_rifle1()
def ne_rifle1():
    print("Pane bože. Takže film vybereš podle sebe a ještě necháš dívku aby za sebe platila?\nTohleto nedopadne. Dívka odchází hned jak začnou závěrečné titulky. Konec hry")
def chytnu_rifle1():
    print("Až na stříkající krev a zombíky byl večer perfektní.\nDívka ti na prahu domu šeptá číslo do ucha a odchází.\nTakže v podstatě výhra, máš co jsi chtěl.")
def nechytnu_rifle1():
    print("Dívka je zklamaná. Kvůli shlédnutému hororu se po cestě domů bála a ty jsi ji ani nechytl za ruku.\nJejí číslo jsi nezískal. Konec hry")

print('Vítej v mé hře!')
time.sleep(1.50)
while True:
    age = input("Kolik je ti let?")
    try:
        age = int(age)
    except ValueError:
        print("Tohle není číslo!")
    else:
        if age >= 15 and age <=50:
            print("Super! Splňuješ požadavky pro hraní této hry. Začínáme!")
            print("+-------------+")
            print("| Rande v kině|")
            print("+-------------+")
            start()
            break
        elif age >51:
            print("Na hraní této hry jsi příliš starý, nemyslíš?")
            sys.exit("Vypínám consoli!")
            break
        else:
            print("Na hraní této hry jsi ještě moc mladý!")
            sys.exit("Vypínám consoli!")

